import "@ungap/custom-elements";

import dispatcher from "./modules/dispatcher";

// webcomponents
import "./components/preloader/preloader.webcomponent";
import "./components/spiral/spiralCanvas.webcomponent";
import "./components/content-container/contentContainer.webcomponent";

// modules
import sandbox from "./modules/sandbox/sandbox";
import TextAnimations from "./modules/animations/text.animations";

const init = () => {
    dispatcher.dispatch("page-load", { type: "load" });

    sandbox.init();

    new TextAnimations(".js-text", {
        threshold: 0.1,
    });
};

if (document.readyState === "loading") {
    document.addEventListener("DOMContentLoaded", init);
} else {
    init();
}
