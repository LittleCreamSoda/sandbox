export type Primitive = string | number | boolean | null | undefined;

export type Event = {
    type: string;
    [key: string]: unknown;
};

export type EmitterFunction = (e: Event) => void;

export type OptionsProperty = string | number | boolean | undefined;

export type QueryElements = NodeListOf<
    HTMLElementTagNameMap[keyof HTMLElementTagNameMap]
>;
