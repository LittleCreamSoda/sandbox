import TweenLite from "gsap";

import "../../../styles/components/content-container.scss";

import scrollStore from "../scroll/scroll.store";
import dispatcher from "../../modules/dispatcher";
import { Event as EmittedEvent } from "../../commonTypes/commonTypes";
import { Bounding } from "../scroll/scroll.types";

class ContentContainer extends HTMLElement {
    #transformed = 0;
    #bounding: Bounding = {
        top: 0,
        bottom: 0,
    };

    constructor() {
        super();
        this.init.call(this);
    }

    showContainer(): void {
        this.classList.add("is-visible");
    }

    transformContainer(): void {
        TweenLite.to(this, 0.6, {
            y: this.#transformed,
        });
    }

    handleScroll(): void {
        const scrollData = scrollStore.getData();
        this.#transformed = -scrollData.scrolled;

        this.transformContainer();
    }

    setBounding(): Promise<void> {
        return new Promise((resolve) => {
            // TODO почему-то без таймаута неправильно нижние границы определяет
            setTimeout(() => {
                const boundingRect = this.getBoundingClientRect();

                this.#bounding = {
                    top: boundingRect.top - this.#transformed,
                    bottom:
                        this.getBoundingClientRect().bottom - this.#transformed,
                };

                resolve();
            }, 50);
        });
    }

    async handleResize(): Promise<void> {
        await this.setBounding();

        dispatcher.dispatch("scroll", {
            type: "update-bounding",
            bottom: this.#bounding.bottom - innerHeight,
        });
    }

    handleEvents(e: EmittedEvent): void {
        switch (e.type) {
            case "finished":
                this.showContainer();
                break;

            default:
                break;
        }
    }

    connectedCallback() {
        this.handleResize().catch((error) => console.error(error));

        dispatcher.subscribe("preloader", this.handleEvents);
        scrollStore.eventEmitter.subscribe(this.handleScroll);
        window.addEventListener("resize", this.handleResize);
    }

    disconnectedCallback() {
        dispatcher.unsubscribe("preloader", this.handleEvents);
        scrollStore.eventEmitter.unsubscribe(this.handleScroll);
        window.removeEventListener("resize", this.handleResize);
    }

    init(): void {
        this.handleEvents = this.handleEvents.bind(this);
        this.handleScroll = this.handleScroll.bind(this);
        this.handleResize = this.handleResize.bind(this);
    }
}

customElements.define("content-container", ContentContainer);
