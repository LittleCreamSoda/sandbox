import EventEmitter from "../../modules/utils/EventEmitter";
import { Options } from "./intersection.types";
import { EmitterFunction, QueryElements } from "../../commonTypes/commonTypes";

interface IntersectionStoreInterface {
    addElements(elements: QueryElements | HTMLElement): void;
    removeElements(elements: QueryElements | HTMLElement): void;
    subscribe(callback: EmitterFunction): void;
    unsubscribe(callback: EmitterFunction): void;
}

class IntersectionStore implements IntersectionStoreInterface {
    #intersectionObserver: IntersectionObserver;
    #eventEmitter: EventEmitter = new EventEmitter();
    readonly #options: Options = {
        root: null,
        rootMargin: "0px",
        threshold: 1.0,
    };

    constructor(options: Options) {
        this.#options = { ...this.#options, ...options };
        this.#intersectionObserver = new IntersectionObserver(
            this._handleChange.bind(this),
            this.#options
        );
    }

    private _emmitIntersection(element: HTMLElement) {
        const mode = element.dataset.observe;

        if (mode === "once") {
            this.#intersectionObserver.unobserve(element);
        }

        this.#eventEmitter.dispatch("intersection", {
            type: "in-viewport",
            element: element,
        });
    }

    private _emmitDisappearance(element: HTMLElement) {
        this.#eventEmitter.dispatch("intersection", {
            type: "out-viewport",
            element: element,
        });
    }

    private _handleEntry(entry: IntersectionObserverEntry): void {
        !entry.isIntersecting
            ? this._emmitDisappearance(<HTMLElement>entry.target)
            : this._emmitIntersection(<HTMLElement>entry.target);
    }

    private _handleChange(entries: IntersectionObserverEntry[]): void {
        entries.forEach((entry) => this._handleEntry(entry));
    }

    public addElements(elements: QueryElements | HTMLElement): void {
        if (elements instanceof HTMLElement) {
            this.#intersectionObserver.observe(elements);
        } else {
            elements.forEach((element) => {
                this.#intersectionObserver.observe(element);
            });
        }
    }

    public removeElements(elements: QueryElements | HTMLElement): void {
        if (elements instanceof HTMLElement) {
            this.#intersectionObserver.unobserve(elements);
        } else {
            elements.forEach((element) => {
                this.#intersectionObserver.unobserve(element);
            });
        }
    }

    public subscribe(callback: EmitterFunction): void {
        this.#eventEmitter.subscribe(callback);
    }

    public unsubscribe(callback: EmitterFunction): void {
        this.#eventEmitter.unsubscribe(callback);
    }
}

export default IntersectionStore;
