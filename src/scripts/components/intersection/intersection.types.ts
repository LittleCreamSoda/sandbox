export type Options = {
    root?: HTMLElement | null;
    rootMargin?: string;
    threshold?: number | Array<number>;
};
