import dispatcher from "../../modules/dispatcher";

// imitation
class Preloader extends HTMLElement {
    constructor() {
        super();
    }

    finishAnimation(): void {
        dispatcher.dispatch("preloader", { type: "finished" });
    }

    startAnimation(): void {
        setTimeout(() => {
            this.finishAnimation();
        }, 1000);
    }

    connectedCallback() {
        this.startAnimation();
    }

    disconnectedCallback() {
        console.log("disconnected");
    }
}

customElements.define("preloader-component", Preloader);
