import dispatcher from "../../modules/dispatcher";
import EventEmitter from "../../modules/utils/EventEmitter";
import type { Event } from "../../commonTypes/commonTypes";
import type {
    ScrollStoreData,
    Bounding,
    MouseWheelEvent,
} from "./scroll.types";

class ScrollStore {
    static bonding: Bounding = {
        top: 0,
        bottom: innerHeight,
    };

    readonly eventEmitter: EventEmitter;
    #scrolled = 0;
    #blocked = false;
    #scrollTimer: ReturnType<typeof setTimeout> = setTimeout(() => {
        return;
    }, 0);

    constructor() {
        this.eventEmitter = new EventEmitter();
        this.init.call(this);
    }

    getData(): ScrollStoreData {
        return {
            scrolled: this.#scrolled,
            blocked: this.#blocked,
        };
    }

    private handleEvents(e: Event): void {
        const type = e.type;

        switch (type) {
            case "update-bounding":
                ScrollStore.bonding.bottom = e.bottom as number;
                this.handleScroll({ deltaY: 0 });
                break;

            case "block":
                this.#blocked = true;
                break;

            case "unblock":
                this.#blocked = false;
                break;

            default:
                break;
        }
    }

    private resetTimer(): void {
        clearTimeout(this.#scrollTimer);
    }

    private handleScroll(e: MouseWheelEvent): void {
        this.resetTimer();
        if (this.#blocked) return;

        this.#scrolled += e.deltaY;

        if (this.#scrolled < ScrollStore.bonding.top)
            this.#scrolled = ScrollStore.bonding.top;

        if (this.#scrolled > ScrollStore.bonding.bottom)
            this.#scrolled = ScrollStore.bonding.bottom;

        this.eventEmitter.dispatch();
    }

    private init(): void {
        this.getData = this.getData.bind(this);
        this.handleEvents = this.handleEvents.bind(this);
        this.handleScroll = this.handleScroll.bind(this);

        dispatcher.subscribe("scroll", this.handleEvents);
        window.addEventListener("wheel", this.handleScroll);
    }
}

const scrollStore = new ScrollStore();

export default {
    eventEmitter: scrollStore.eventEmitter,
    getData: scrollStore.getData,
};
