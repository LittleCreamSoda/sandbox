export type ScrollStoreData = {
    scrolled: number;
    blocked: boolean;
};

export type Bounding = {
    top: number;
    bottom: number;
};

export type MouseWheelEvent = {
    deltaY: number;
};
