type Point = {
    x: number;
    y: number;
};

const drawSpiral = (
    context: CanvasRenderingContext2D,
    width: number,
    height: number,
    color = "#000",
    spiralColor = "#fff"
): void => {
    const center: Point = {
        x: width / 2,
        y: height / 2,
    };

    context.strokeStyle = spiralColor;
    context.fillStyle = color;

    const getRatio = (currentX: number, centerX: number): number => {
        return Math.abs(currentX - centerX) / 160;
    };

    const drawSpiral = (): void => {
        context.clearRect(0, 0, width, height);
        context.beginPath();

        let distance = 1;
        const a = 0;
        let angle = 0;
        let x = 0;
        let y = 0;

        while (x >= 0 || y >= 0) {
            x = center.x + (a + distance * angle) * Math.sin(angle);
            y = center.y + (a + distance * angle) * Math.cos(angle);
            context.lineTo(x, y);
            angle += Math.PI / 30;

            const ratio = getRatio(x, center.x);
            context.lineWidth = ratio;
            distance += 0.004;
        }

        context.fillRect(0, 0, width, height);
        context.stroke();
    };

    const rotate = (): void => {
        requestAnimationFrame(() => {
            drawSpiral();
            // rotate();
        });
    };

    rotate();
};

export default drawSpiral;
