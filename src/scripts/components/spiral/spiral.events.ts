export type AddEvent = {
    type: "add";
    color: string;
    spiralColor: string;
};

export type ChangeColorEvent = {
    type: "change-color";
    color?: string;
    spiralColor: string;
};
