import dispatcher from "../../modules/dispatcher";
import EventEmitter from "../../modules/utils/EventEmitter";
import type { Event } from "../../commonTypes/commonTypes";

type Spiral = {
    color: string;
    spiralColor: string;
};

let spiral: Spiral;

const eventEmitter: EventEmitter = new EventEmitter();

const handleEvent = (e: Event): void => {
    switch (e.type) {
        case "add":
            spiral = {
                color: e.color as string,
                spiralColor: e.spiralColor as string,
            };
            break;

        case "remove":
            spiral = {
                color: "",
                spiralColor: "",
            };
            break;

        case "change-color":
            if (e.color && typeof e.color === "string") spiral.color = e.color;

            if (e.spiralColor && typeof e.spiralColor === "string")
                spiral.spiralColor = e.spiralColor;

            eventEmitter.dispatch();
            break;

        default:
            break;
    }
};

const getData = (): Spiral => {
    return {
        color: spiral.color,
        spiralColor: spiral.spiralColor,
    };
};

const init = (): void => {
    dispatcher.subscribe("spiral", handleEvent);
};

init();

export default {
    subscribe: eventEmitter.subscribe.bind(eventEmitter),
    unsubscribe: eventEmitter.unsubscribe.bind(eventEmitter),
    getData: getData,
};
