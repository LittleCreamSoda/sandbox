import "../../../styles/components/spiral.scss";

import spiralStore from "./spiral.store";
import dispatcher from "../../modules/dispatcher";
import datasetToOptions from "../../modules/utils/component.utils/datasetToOptions";
import { OptionsProperty } from "../../commonTypes/commonTypes";
import { AddEvent } from "./spiral.events";
import drawSpiral from "./drawSpiral";

interface SpiralInterface {
    context: CanvasRenderingContext2D | null;
}

const defaultOptions = {
    __namespace: "",
    color: "#000",
    spiralColor: "#fff",
};

class Spiral extends HTMLCanvasElement implements SpiralInterface {
    context: CanvasRenderingContext2D | null = null;
    #options: {
        [key: string]: OptionsProperty;
    };

    constructor() {
        super();
        this.#options = {};
        this.init.call(this);
    }

    private handleChange(): void {
        const storeData = spiralStore.getData();
        this.#options.color = storeData.color;
        this.#options.spiralColor = storeData.spiralColor;
        this.drawCanvas();
    }

    drawCanvas(): void {
        if (!this.context) {
            throw new Error("no context provided");
        }

        this.width = innerWidth;
        this.height = innerHeight;

        this.context.fillStyle =
            (this.#options.color as string) ?? defaultOptions.color;
        this.context.fillRect(0, 0, this.width, this.height);

        drawSpiral(
            this.context,
            this.width,
            this.height,
            this.#options.color as string,
            this.#options.spiralColor as string
        );
    }

    private setComponent(): void {
        this.context = this.getContext("2d") ?? null;
        this.width = innerWidth;
        this.height = innerHeight;
    }

    connectedCallback(): void {
        this.#options = datasetToOptions(this.dataset, defaultOptions);
        this.setComponent();
        this.drawCanvas();

        const addEvent: AddEvent = {
            type: "add",
            color: this.#options.color as string,
            spiralColor: this.#options.spiralColor as string,
        };

        dispatcher.dispatch("spiral", addEvent);
        spiralStore.subscribe(this.handleChange);
        window.addEventListener("resize", this.drawCanvas);
    }

    disconnectedCallback(): void {
        spiralStore.unsubscribe(this.handleChange);
        dispatcher.dispatch("spiral", { type: "remove" });
        window.removeEventListener("resize", this.drawCanvas);
    }

    private init(): void {
        this.handleChange = this.handleChange.bind(this);
        this.drawCanvas = this.drawCanvas.bind(this);
    }
}

customElements.define("spiral-canvas", Spiral, { extends: "canvas" });
