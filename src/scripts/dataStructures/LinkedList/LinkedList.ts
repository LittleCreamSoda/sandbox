import LinkedListElement, { ListElementType } from "./LinkedListElement";

interface LinkedListInterface {
    length: number;
    head: ListElementType;
    tail: ListElementType;
    append<T>(element: T): T;
    prepend<T>(element: T): T;
    search<T>(element: T): boolean;
    delete<T>(element: T): void;
    toArray(): Array<unknown>;
}

export default class LinkedList implements LinkedListInterface {
    private _head: ListElementType;
    private _tail: ListElementType;
    length: number;

    constructor(values: Array<unknown> = []) {
        this._head = null;
        this._tail = null;
        this.length = 0;

        values.forEach((value: unknown) => {
            this.append(value);
        });
    }

    get head(): ListElementType {
        return this._head;
    }

    get tail(): ListElementType {
        return this._tail;
    }

    append<T>(element: T): T {
        const listElement: LinkedListElement = new LinkedListElement(element);

        if (!this._head || !this._tail) {
            this._head = listElement;
            this._tail = listElement;
        } else {
            this._tail.next = listElement;
            this._tail = listElement;
        }

        this.length++;

        return element;
    }

    prepend<T>(element: T): T {
        const listElement: LinkedListElement = new LinkedListElement(element);

        if (!this._head) {
            this._head = listElement;
            this._tail = listElement;
        } else {
            listElement.next = this._head;
            this._head = listElement;
        }

        this.length++;

        return element;
    }

    search<T>(element: T): boolean {
        let isFound = false;

        if (!this._head) {
            isFound = false;
        } else {
            let current: ListElementType = this._head;

            while (current) {
                if (current.value === element) {
                    isFound = true;
                } else if (!current.next) {
                    isFound = false;
                }

                current = current.next;
            }
        }

        return isFound;
    }

    delete<T>(element: T): void {
        if (!this._head) {
            console.warn("no elements in list");
        } else {
            let current: ListElementType = this._head;

            while (current) {
                if (current.value === element && current === this._head) {
                    this._head = current.next;
                    this.length--;
                } else if (current.value === element) {
                    current.next
                        ? (current.next = current.next.next)
                        : this.cutTail();
                    break;
                }

                current = current.next;
            }
        }
    }

    private cutTail(): void {
        let current: ListElementType = this._head;

        while (current) {
            if (current.next?.next === null) {
                current.next = null;
                this.length--;
                break;
            }

            current = current.next;
        }
    }

    toArray(): Array<unknown> {
        const listArray: Array<unknown> = [];

        if (!this._head) {
            return listArray;
        } else {
            let current: ListElementType = this._head;

            while (current) {
                listArray.push(current.value);
                current = current.next;
            }

            return listArray;
        }
    }
}
