export type ListElementType = LinkedListElement | null;

interface LinkedListElementInterface {
    value: unknown;
    next: LinkedListElement | null;
}

export default class LinkedListElement implements LinkedListElementInterface {
    value: unknown;
    next: ListElementType;

    constructor(value: unknown, next: ListElementType = null) {
        this.value = value;
        this.next = next;
    }
}
