import LinkedList from "../LinkedList/LinkedList";

interface QueueInterface {
    length: number;
    push(element: unknown): unknown;
    delete(element: unknown): void;
    shift(): unknown;
    toArray(): Array<unknown>;
}

export default class Queue implements QueueInterface {
    length: number;
    private linkedList: LinkedList;

    constructor(values: Array<unknown> = []) {
        this.linkedList = new LinkedList(values);
        this.length = this.linkedList.length;
    }

    push<T>(element: T): T {
        this.linkedList.append(element);
        this.updateLength();

        return element;
    }

    shift(): unknown {
        const firstValue: unknown = this.linkedList.head?.value;
        this.linkedList.delete(firstValue);

        return firstValue;
    }

    delete<T>(element: T): void {
        this.linkedList.delete(element);
        this.updateLength();
    }

    private updateLength(): void {
        this.length = this.linkedList.length;
    }

    toArray(): Array<unknown> {
        return this.linkedList.toArray();
    }
}
