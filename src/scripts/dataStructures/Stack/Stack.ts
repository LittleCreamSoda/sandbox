import LinkedList from "../LinkedList/LinkedList";
import { ListElementType } from "../LinkedList/LinkedListElement";

interface StackInterface {
    length: number;
    push(element: unknown): unknown;
    delete(element: unknown): void;
    pop(): unknown;
    toArray(): Array<unknown>;
}

export default class Stack implements StackInterface {
    length: number;
    private linkedList: LinkedList;

    constructor(values: Array<unknown> = []) {
        this.linkedList = new LinkedList(values);
        this.length = this.linkedList.length;
    }

    push<T>(element: T): T {
        this.linkedList.append(element);
        this.updateLength();

        return element;
    }

    delete<T>(element: T): void {
        this.linkedList.delete(element);
        this.updateLength();
    }

    private updateLength(): void {
        this.length = this.linkedList.length;
    }

    pop(): unknown {
        let popValue: unknown = null;
        let current: ListElementType = this.linkedList.head;

        if (current) {
            while (current) {
                if (!current.next) {
                    popValue = current.value;
                    this.delete(popValue);
                }

                current = current.next;
            }
        }

        return popValue;
    }

    toArray(): Array<unknown> {
        return this.linkedList.toArray();
    }
}
