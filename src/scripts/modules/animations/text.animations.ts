import IntersectionStore from "../../components/intersection/intersection.store";
import { QueryElements, Event } from "../../commonTypes/commonTypes";
import hasOwnProperty from "../utils/object.utils/hasOwnProperty";

interface TextAnimationsInterface {
    items: QueryElements | [];
    store: IntersectionStore;
    selector: string;
}

class TextAnimations implements TextAnimationsInterface {
    items: QueryElements | [] = [];
    store: IntersectionStore;
    selector: string;

    constructor(selector = ".js-text", options = {}) {
        this.selector = selector;
        this.store = new IntersectionStore(options);
        this._observeElements();

        this.store.subscribe(this._handleChange);
    }

    private _observeElements(): void {
        this.items = document.querySelectorAll(this.selector);
        this.store.addElements(this.items);
    }

    private _handleChange(e: Event) {
        if (hasOwnProperty(e, "element")) {
            const element = e.element;

            if (element instanceof HTMLElement) {
                e.type === "out-viewport"
                    ? element.classList.remove("_is-visible")
                    : element.classList.add("_is-visible");
            }
        }
    }
}

export default TextAnimations;
