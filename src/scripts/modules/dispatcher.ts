import EventEmitter from "./utils/EventEmitter";

const dispatcher: EventEmitter = new EventEmitter();

export default dispatcher;
