import type { Event, EmitterFunction } from "../../commonTypes/commonTypes";

type Handler = {
    channel: string;
    to?: "add" | "remove";
    handler: EmitterFunction;
};

interface EventEmitterInterface {
    dispatch(channel: string | Event, event: Event | null): void;

    subscribe(
        channel: string | EmitterFunction,
        handler: EmitterFunction | null
    ): void;
    unsubscribe(
        channel: string | EmitterFunction,
        handler: EmitterFunction | null
    ): void;
}

class EventEmitter implements EventEmitterInterface {
    readonly #handlers: {
        [channel: string]: Array<EmitterFunction>;
        all: Array<EmitterFunction>;
    };
    #pendingHandlers: Array<Handler>;
    #frozen: number;

    constructor() {
        this.#handlers = {
            all: [],
        };

        this.#pendingHandlers = [];

        this.#frozen = 0;
    }

    dispatch(
        inputChannel: string | Event = "all",
        inputEvent: Event | null = null
    ): void {
        let channel: string;
        let event: Event;

        if (inputEvent) {
            channel = inputChannel as string;
            event = inputEvent;
        } else {
            channel = "all";
            event = inputChannel as Event;
        }

        if (event.type && event.type.indexOf(":") !== -1) {
            channel = event.type.split(":")[0];
        }

        if (!this.#handlers[channel]) {
            this.#handlers[channel] = [];
        }

        this.#frozen++;

        this.#handlers[channel].forEach((handler: EmitterFunction): void => {
            handler(event);
        });

        if (channel !== "all") {
            this.#handlers["all"].forEach((handler: EmitterFunction) => {
                handler(event);
            });
        }

        this.#frozen--;

        if (!this.#frozen) {
            this.#pendingHandlers.forEach((h: Handler) => {
                if (h.to === "add") {
                    this.subscribe(h.channel, h.handler);
                } else if (h.to === "remove") {
                    this.unsubscribe(h.channel, h.handler);
                }
            });

            this.#pendingHandlers = [];
        }
        if (this.#frozen < 0) {
            console.warn("something weird just happened");
        }
    }

    subscribe(
        inputChannel: string | EmitterFunction,
        inputHandler: EmitterFunction | null = null
    ): void {
        let channel = "all";
        let handler: EmitterFunction;

        if (!inputHandler) {
            handler = inputChannel as EmitterFunction;
        } else {
            channel = inputChannel as string;
            handler = inputHandler;
        }

        if (this.#frozen) {
            this.#pendingHandlers.push({
                channel: channel,
                handler: handler,
                to: "add",
            });

            return;
        }

        if (typeof handler !== "function") {
            console.error("handler has to be a function");
            return;
        }

        if (!this.#handlers[channel]) {
            this.#handlers[channel] = [];
        }

        if (this.#handlers[channel].indexOf(handler) === -1) {
            this.#handlers[channel].push(handler);
        } else {
            console.error(handler, "handler already set");
        }
    }

    unsubscribe(
        inputChannel: string | EmitterFunction,
        inputHandler: EmitterFunction | null = null
    ): void {
        let channel = "all";
        let handler: EmitterFunction;

        if (!inputHandler) {
            handler = inputChannel as EmitterFunction;
            channel = "all";
        } else {
            handler = inputHandler;
        }

        if (this.#frozen) {
            this.#pendingHandlers.push({
                channel: channel,
                handler: handler,
                to: "remove",
            });
            return;
        }

        if (typeof handler !== "function") {
            console.error("handler has to be a function");
            return;
        }

        if (!this.#handlers[channel]) {
            console.error("channel " + channel + "  does not exist");
            return;
        }

        if (this.#handlers[channel].indexOf(handler) === -1) {
            console.error(handler, "trying to unsubscribe unexisting handler");
            return;
        }

        this.#handlers[channel] = this.#handlers[channel].filter(function (h) {
            return h !== handler;
        });
    }
}

export default EventEmitter;
