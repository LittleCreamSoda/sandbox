const camelize = (str: string): string => {
    return str.replace(
        /(?:^\w|[A-Z]|\b\w|\s+)/g,
        (match: string, index: number) => {
            if (+match === 0) return "";
            return index == 0 ? match.toLowerCase() : match.toUpperCase();
        }
    );
};

type datasetType = {
    [key: string]: string | number | boolean | undefined;
};

const datasetToOptions = (
    dataset: datasetType,
    options: datasetType
): datasetType => {
    const result: datasetType = {};
    const namespace = options.__namespace ?? "";

    Object.keys(options).forEach((key) => {
        let camelKey;
        if (options.__namespace) {
            camelKey = camelize(namespace + " " + key);
        } else {
            camelKey = camelize(key);
        }

        if (!dataset[camelKey]) {
            result[key] = options[key];
        } else if (typeof options[key] === "number") {
            result[key] = parseInt(dataset[camelKey] as string);
        } else if (typeof options[key] === "boolean") {
            result[key] = dataset[camelKey] === "true";
        } else {
            result[key] = dataset[camelKey];
        }
    });

    return result;
};

export default datasetToOptions;
