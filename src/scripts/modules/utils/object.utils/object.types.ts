type UnknownObject = {
    [key: string]: unknown;
};
