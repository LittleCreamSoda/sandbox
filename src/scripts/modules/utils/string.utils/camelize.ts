import capitalize from "./capitalize";

const camelize = (text: string, separator = "_"): string => {
    if (typeof text !== "string") return text;

    const words = text.split(separator);
    const result = [words[0]];
    words.slice(1).forEach((word) => result.push(capitalize(word)));
    return result.join("");
};

export default camelize;
